App.room = App.cable.subscriptions.create "RoomChannel",
  connected: ->
    # Called when the subscription is ready for use on the server

  disconnected: ->
    # Called when the subscription has been terminated by the server

  received: (data) ->
    $("#" + data['active_box']).removeAttr('class')
    $("#" + data['active_box']).addClass(data['message'])
    $("#" + data['active_box']).addClass("square_box")

  speak: (message, active_box) ->
    @perform 'speak', message: message, active_box: active_box

  current_color = ''
  $(document).ready ->
    $("#color_palletes").children().click (event) ->
      current_color = $(this).attr('class')


    $('#board').children().click (event)->
      App.room.speak(current_color, $(this).attr("id"))



